#!/usr/bin/python
'''
Cloudflare DNS configuration Script

Copyright (c) 2017 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.

Cloudflare is a registered trademark of Cloudflare, Inc.
'''
import CloudFlare
import argparse
import os
from ConfigParser import SafeConfigParser

if os.environ.get('CF_CONFIG_FILE') is None:
    print('Please set the CF_CONFIG_FILE variable')
    exit()

try:
    with open(os.environ['CF_CONFIG_FILE']) as file:
        pass
except IOError as e:
        print('Unable to open file')
        exit()

configparse = SafeConfigParser()
configparse.read(os.environ['CF_CONFIG_FILE'])


argparser = argparse.ArgumentParser(description='Query Cloudflare API')
argparser.add_argument('-a', dest='addhost', help='Add domain', action='store')
argparser.add_argument('-i', dest='ipaddr', action='store')
argparser.add_argument('-d', dest='domain', action='store')
argparser.add_argument('-r', dest='delhost', action='store', help='Delete host from Cloudflare')
argparser.add_argument('-l', dest='list', help='List some settings in a Cloudflare account', action='store')
args = argparser.parse_args()

CF_MAIL = configparse.get('CF', 'Mail')
CF_API_KEY = configparse.get('CF', 'API-KEY')
cf = CloudFlare.CloudFlare(CF_MAIL, CF_API_KEY)
zones = cf.zones.get()

# define functions
def getDomainID(domain_name):
    for y in zones:
        if y['name'] == domain_name:
            zone_id = y['id']
    return zone_id

def getRecordID(record_name):
    get_records = cf.zones.dns_records.get(getDomainID(args.domain))
    for x in get_records:
        if x['name'] == record_name:
            x_id = x['id']
    return x_id

try:
    if args.list == 'listzones':
        for i in zones:
            zoneName = i['name']
            zoneId = i['id']
            zoneStatus = i['status']
            get_settings_ipv6 = cf.zones.settings.ipv6.get(zoneId)
            ipv6_on = get_settings_ipv6['value']
            print 'Zone Name:', zoneName, '\nZone ID:', zoneId, '\nIPv6:',\
                ipv6_on, '\nStatus:', zoneStatus, '\n'

    elif args.list == 'getdnsrecords':
        get_records = cf.zones.dns_records.get(getDomainID(args.domain))
        for x in get_records:
            r_name = x['name']
            r_type = x['type']
            r_value = x['content']
            r_id = x['id']
            r_proxied = x['proxied']
            print '\t', r_type, r_name, r_value, r_id, 'CDN active:', r_proxied

    elif args.addhost:
        # Init dict
        dns_records = []
        dns_records.append({'name':args.addhost, 'type':'A', 'content':args.ipaddr, 'proxiable':'False'})
        if args.domain:
            for x in dns_records:
                r = cf.zones.dns_records.post(getDomainID(args.domain), data=x)
                print '\nSuccessful added!', '\n', 'Name:', args.addhost, '\n',\
                    'IPADDR:', args.ipaddr

        else:
            print 'Specify zone name!'
    elif args.delhost:
         while True:
             print 'Choose [y/n]'
             confirm_delhost = raw_input('Delete ' + args.delhost + '? [y/n] ')
             if not confirm_delhost == '':
                break
         if confirm_delhost == 'y':
            cf.zones.dns_records.delete(getDomainID(args.domain), getRecordID(args.delhost))
            print '\nSuccessful deleted!'

except (KeyboardInterrupt, SystemExit):
    print "Exiting"
    exit(255)

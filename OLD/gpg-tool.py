#!/usr/bin/python
'''
Copyright (c) 2017 Benedikt Toelle <btoelle@rfc3091.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''

import argparse
import gnupg
import os
from ConfigParser import SafeConfigParser


if os.environ.get('GPG_TOOL_CONFIGFILE') is None:
    print('Please GPG_TOOL_CONFIGFILE variable')
    exit()


try:
    with open(os.environ['GPG_TOOL_CONFIGFILE']) as file:
        pass
except IOError as e:
    print('Unable to open file')
    exit()

configparse = SafeConfigParser()
configparse.read(os.environ['GPG_TOOL_CONFIGFILE'])

gpg = gnupg.GPG(verbose=True, gpgbinary='/usr/bin/gpg2', gnupghome=configparse.get('gpg-tool','gpg-homedir'))
gpg.encoding = 'utf-8'

# functions

argparser = argparse.ArgumentParser(description='Do some gpg operations')
argparser.add_argument('-l', dest='list_keys', help='List gpg keys', action='store_true')
#argparser.add_argument('-e', dest='encrypt_file', help='Encrypt file', action='store')  
args = argparser.parse_args()

try:

    if args.list_keys:
        gpg.list_keys()

except(KeyboardInterrupt, SystemError):
    print("Exiting")
    exit(255)



#elif args.encrypt
# Logic here


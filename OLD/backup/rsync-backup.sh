#!/bin/bash
: '
Copyright (c) 2016 Benedikt Toelle <btoelle@btoelle.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'

if ! [ "$EUID" == "0" ]; then
	echo "This script is not running by root"
	echo "Exiting"
	exit 0
fi


if [ -n "$1" ]; then
	SOURCEPC="$1"
else
	echo "Simple rsync backup script"
	echo "Copyright (c) 2016 Benedikt Toelle <btoelle@btoelle.org>"
	echo "Usage: ./backup.sh <hostname>"
	exit 0
fi

SPATH="/etc /opt /home /usr/share /root /srv"
TARGETPATH=/data/backup/rsync
EXCLUDE=$PWD/exclude.list
EXTRAOPTS=""
SSHOPTS=""
SSHKEYFILEPATH=""
LOG=/tmp/rsync-$(date +%d-%m-%y_%R:%S).log
PKGLIST=pkg-$(date +%d-%m-%y_%R:%S).list

if ping -c 5 $SOURCEPC &> /dev/null
then 
	echo "Good! Host is up"
else
	echo "ERROR: Source host is down!"
	exit 0
fi

if [ -e "$SSHKEYFILEPATH" ]; then
	ssh-add $SSHKEYFILEPATH
else
	echo "No keyfile defined"
	echo "ssh-agent exiting"
fi

do_rsync(){
		touch $LOG
		rsync -avzSAXHRPhE --numeric-ids -e ssh $SSHOPTS --delete --delete-excluded \
				 --backup-dir=$TARGETPATH/$SOURCEPC/rsync-deleted-backup \
				 --exclude-from="$EXCLUDE" $EXTRAOPTS \
				 root@$SOURCEPC:$i $TARGETPATH/$SOURCEPC/hourly.0 >> $LOG 2>&1
		if ! [ $? = 24 -o $? = 0 ]; then 
		 			echo "FATAL ERROR: rsync can not backup $SOURCEPC"
		fi
}

if ! [ -d $TARGETPATH/$SOURCEPC/hourly.0 ]; then
		mkdir -p $TARGETPATH/$SOURCEPC/hourly.0
fi

if ! [ -d $TARGETPATH/$SOURCEPC/rsync-deleted-backup ]; then
		mkdir $TARGETPATH/$SOURCEPC/rsync-deleted-backup
fi

echo "Starting Rsync backup from $SOURCEPC"

for i in $SPATH; do
	do_rsync 
done

echo "Starting package list backup from $SOURCEPC"

ssh root@$SOURCEPC $SSHOPTS dpkg --get-selections | awk '!/deinstall|purge|hold/' | cut -f1 | tr '\n' ' ' > $TARGETPATH/$SOURCEPC/$PKGLIST

if [ -e $TARGETPATH/$SOURCEPC/$PKGLIST ]; then
	echo "FINISHED: package list backup from $SOURCEPC"
else
	echo "FATAL ERROR: He's dead Jim!"
	echo "Please try again with other options!"
	exit 0
fi

if [ $? == 0 ]; then
	echo "FINISHED: rsync backup from $SOURCEPC complete!"
else
	echo "FATAL ERROR: He's dead Jim!"
	echo "Please try again with other options!"
fi

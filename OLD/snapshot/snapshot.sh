#!/bin/bash
: '
Copyright (c) 2016 Benedikt Toelle <btoelle@rfc3091.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'
LIB64V=""
# Is the script run by root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
  echo "Exiting"
	exit 0
fi 
# Is sha512sum installed on the pc
if ! [ -f "/usr/bin/sha512sum" ]; then
	echo "SHA512sum is not existent"
fi
# Is the directory /lib64 present
if [ -d "/lib64" ]; then
	LIB64V="/lib64"
fi


case "$1" in
 create)
	echo "Generate hashsums..."
  find /boot /etc /var /lib /sbin /bin $LIB64V -not -path "/var/log/*" -not -path "/var/cache/*" -type f -print0  | xargs -0 sha512sum > checksums-$(date +%d-%m-%y).sha
  /bin/gzip -9 checksums-$(date +%d-%m-%y).sha
  ;;
 check)
	echo "Check hashsums..."
  # Check if snapshot file compressed
  if [[ $2 =~ \.gz$ ]]; then
	/bin/zcat $2 | /usr/bin/sha512sum -c	
  fi
  /usr/bin/sha512sum -c $2
  ;;
  *)
	echo ""
	echo "Generate a hashsum for each system file and store it in a file"
	echo "Usage:"
	echo "create: create a system snapshot and compress the file"
	echo "check: read hashsums from file and check them"
  exit 1
  ;;
esac

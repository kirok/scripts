'''
Copyright (c) 2018 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''


import mysql.connector


class mysql_connect:

    def __init__(self, commit=False):
        self.commit = commit

    def __enter__(self):
        #Establish sql connection
        self.connection = mysql.connector.connect(unix_socket = "/var/run/mysqld/mysqld.sock",
                                                user = "root",
                                                db  = "vmail")
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exec_type, exec_value, exec_tb):
        if self.commit:
            self.connection.commit()
        self.cursor.close()
        self.connection.close()
        return 

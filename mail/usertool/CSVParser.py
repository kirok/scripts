'''
Copyright (c) 2019 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''

import prettytable
import csv
#import sys
import os
from pathlib import Path


class Parser():
    def __init__(self, filename=None, fromString=None):
        self.filename = filename
        self.fromString = fromString

    def printTable(self):
        path = os.path.join(self.filename)
        conf = Path(path)

        try:
            lc = conf.resolve() #  strict=True for python version >= 3.7
        except FileNotFoundError as e:
            print("File doesn't exists!")
            exit(1)



        table = None
        with open(self.filename) as f:
            content = csv.reader(f, delimiter=',')
            for row in content:
                if table is None:
                    table = prettytable.PrettyTable(row)
                else:
                        table.add_row(row)
            print(table)

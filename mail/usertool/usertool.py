#!/usr/bin/python3
'''
Copyright (c) 2018 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''

from mysql_connect import mysql_connect
from crontab import CronTab
from CryptUserPW import SSHA512Hasher
from genTrashMail import TrashMail
from CSVParser import Parser
import argparse
import datetime
import os
import getpass
import tempfile

ARGPARSER = argparse.ArgumentParser(description='Manage vmail users')
ARGPARSER.add_argument(dest='action', metavar='action')
#ARGPARSER.add_argument(dest='subaction', metavar='subaction')
ARGPARSER.add_argument('-r', '--remove', action='store')
ARGPARSER.add_argument('-d', '--domain', action='store')
ARGPARSER.add_argument('-u', '--user', action='store')
ARGS = ARGPARSER.parse_args()



if ARGS.action == 'adduser':
    print('Add ', ARGS.subaction, 'to Database')

    user_pw = None
    user_pw_confirm = None

    while user_pw != user_pw_confirm or user_pw == None:
        user_pw = getpass.getpass("Enter password: ")
        user_pw_confirm = getpass.getpass("Confirm password: ")
        if user_pw != user_pw_confirm:
            print("\nPassword mismatch")

    hash_pw = SSHA512Hasher().encode(user_pw)

    sql_add_user = """
    INSERT INTO accounts (username, domain, password, quota, enabled, sendonly) values ('%s', '%s', '%s', 2048, true, false);
    """ % (ARGS.user, ARGS.domain, hash_pw)

    with mysql_connect() as cursor:
        cursor.execute(sql_add_user)

elif ARGS.action == 'deluser':
    print('Delete user', ARGS.user, 'from Database')

    sql_del_user = """
    DELETE FROM accounts WHERE username='%s';
    """ % (ARGS.user)

    with mysql_connect() as cursor:
        cursor.execute(sql_del_user)

    print('Account', ARGS.user, 'successfully removed from Database')


elif ARGS.action == 'randomalias':
    TrashMail().create(ARGS.user, ARGS.domain)

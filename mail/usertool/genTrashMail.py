'''
Copyright (c) 2018 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''


from mysql_connect import mysql_connect
from crontab import CronTab
import random
import string
import datetime


class TrashMail:

    def __init__(self):
        self.CRON = CronTab(user=True)

    def create(self,fwd_to, fwd_domain):
        RAND_STR = ''.join(random.choice(string.ascii_lowercase + string.digits) for y in range(10))
        TRASH_MAIL_ADDR = RAND_STR + '@' + fwd_domain
        ALIAS_FWD_TO = fwd_to + '@' + fwd_domain

        # SQL Logic

        sql_add_alias = """
        INSERT INTO aliases (source_username, source_domain, destination_username, destination_domain, enabled) values ('%s','%s', '%s', '%s', true);
        """ % (RAND_STR, fwd_domain, fwd_to, fwd_domain)



        with mysql_connect(commit=True) as cursor:
            cursor.execute(sql_add_alias)

        # Create a CronTab entry

        CRONCMD = """
        python3 /root/bin/usertool.py -t -d %s
        """ % (RAND_STR)

        job = self.CRON.new(command=CRONCMD, comment=TRASH_MAIL_ADDR)
        job.hour.every(5)
        self.CRON.write()
        ALIAS_EXPIRE_DATETIME = datetime.datetime.now() + datetime.timedelta(hours = 5)
        INFO = """
Random Alias: %s
Forwarded To: %s
Alias expires on: %s
        """ % (TRASH_MAIL_ADDR, ALIAS_FWD_TO, ALIAS_EXPIRE_DATETIME)
        print(INFO)


    def delete(self, trash_mail_addr, domain_name):
        sql_del_alias = """
        DELETE FROM aliases WHERE source_username='%s';
        """ % (trash_mail_addr)
        ALIAS_CRON_COMMENT = trash_mail_addr + '@' + domain_name

        with mysql_connect(commit=True) as cursor:
            cursor.execute(sql_del_alias)

        # Remove crontab entry

        for x in self.CRON:
            if x.comment == ALIAS_CRON_COMMENT:
                self.CRON.remove(x)
                self.CRON.write()
        return 'Alias ' + trash_mail_addr  + ' successfully removed!'

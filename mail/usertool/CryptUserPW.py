'''
Module for usertool
Generates a SHA512-CRYPT Hashsum from user input

Copyright (c) 2018 Benedikt Toelle <btoelle@rfc5514.org>

This file is part of Linux-Scripts.

Linux-Scripts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License

Linux-Scripts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Linux-Scripts.  If not, see <http://www.gnu.org/licenses/>.
'''

import getpass
import crypt

class SSHA512Hasher:
    def encode(self, password):
        encode_pw = crypt.crypt(password, crypt.mksalt(crypt.METHOD_SHA512))
        return '{SHA512-CRYPT}' + encode_pw
     

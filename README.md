# Various Linux Scripts

**Maintainer: Benedikt Toelle <btoelle@rfc5514.org>**  

**Report bugs to <bugs@rfc5514.org>**  

### License
All files are licensed under the GNU General Public License Version 3


### misc - Some scripts
* random_port.py  - Generate a random number from 45500-65500  
* fai-chboot.py   - Create a custom tftp config for the FAI installation system 
* cf_dnsconfig.py - DNS configuration script for CloudFlare (WIP)
* blocklist	  - Block country IP subnets with ipset
* mysqldump_to_csv - Dump sql tables to a csv file

### mail - Some scripts for linux mail servers
* usertool        - Manage vmail SQL Database (WIP)
* dovecot	  - useful dovecot scripts

